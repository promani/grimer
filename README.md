Grimer: A payment mock server by Symfony
========================================
Most of mock servers only handle a simple response from a request. Some also allow to handle diferent responses according to conditions. But... i did't found one that make a request callback for the result of a operation and mock the first answer and further callback: Grimer pretend that. Also has a credit card tokenizer like PCI complience plataform Spreedly form work as a proxy and replace data in request and handle the response from a receiver.

Requirements
------------
  * PHP 8.0.4 or higher;
  * and the [usual Symfony application requirements][2].

Usage
-----

TBC

Tests
-----

Execute this command to run tests:

```bash
$ cd my_project/
$ ./bin/phpunit
```
