<?php

namespace App\Controller;

use App\Entity\Mock;
use App\Entity\Service;
use App\Message\DebinMessage;
use App\Service\MockHandler;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/services', name: 'service_')]
class ServiceController extends AbstractController
{
    #[Route('/coelsa/{env}/{path}', name: 'debin', requirements:["path" => ".+"])]
    public function debin(Request $request, ManagerRegistry $registry, MockHandler $mockHandler, MessageBusInterface $bus): Response
    {
        $coelsa = $registry->getRepository(Service::class)->find('coelsa');

        $response = $mockHandler->handleServiceMocks($coelsa, $request);

        if ($response->getStatusCode() > 299) return $response;

        $debin = json_decode($request->getContent(), true);

        if (!$debin['operacion']['detalle']) throw new UnprocessableEntityHttpException('Missing detalle');
        if (!$debin['operacion']['detalle']['qr_id_trx']) throw new UnprocessableEntityHttpException('Missing qr_id_trx');
        $debin['operacion']['id'] = json_decode($response->getContent())->debin->id;

        $url = $_ENV['DEBIN_WEBHOOK_URL_'.$request->get('env')] ?? $_ENV['DEBIN_WEBHOOK_URL'];
        $bus->dispatch(new DebinMessage($debin, $url));

        return $response;
    }

	#[Route('/{id}/{path}', name: 'request', requirements:["path" => ".+"])]
	public function request(Request $request, Service $service, MockHandler $mockHandler): Response
	{
        return $mockHandler->handleServiceMocks($service, $request);
	}

}
