<?php

namespace App\Controller;

use App\Entity\Card;
use App\Entity\Service;
use App\Service\MockHandler;
use App\Model\Tokenizer\Deliver;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[Route('/tokenizer', name: 'tokenizer_')]
class TokenizerController extends AbstractController
{
	#[Route('/receivers/{id}/deliver', name: 'deliver')]
	public function deliver(Request $originalRequest, HttpClientInterface $client, Service $service, MockHandler $mockHandler,ManagerRegistry $em): Response
	{
		$serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
		/** @var Deliver $deliver */
		$deliver = $serializer->deserialize($originalRequest->getContent(), Deliver::class, 'json');
		if ($service->isMocked()) {
			$request = new Request(
				$originalRequest->query->all(),
				['headers' => $deliver->headers],
				$originalRequest->attributes->all(),
				[],
				[],
				['REQUEST_URI' => $deliver->url],
				json_encode($deliver->body)
			);
			$request->setMethod($deliver->method);
			return $mockHandler->handleServiceMocks($service, $request);
		} else {
			$card = $em->getRepository(Card::class)->find($deliver->paymentMethodToken);
			array_walk_recursive($deliver->body, function (&$value) use ($card) {
				$value = strtr($value, ['{{credit_card_number}}' => $card->getPan()]);
			});

			$reponse = $client->request($deliver->method, $deliver->url, [
				'headers' => $deliver->headers,
				'body' => $deliver->body
			]);

			return new JsonResponse($reponse->getContent(), $reponse->getStatusCode());
		}
	}
}
