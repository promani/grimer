<?php

namespace App\Controller\Admin;

use App\Entity\Card;
use App\Entity\Mock;
use App\Entity\Service;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Route('/admin', name: 'admin_')]
class DashboardController extends AbstractDashboardController
{
	#[Route('/', name: 'dashboard')]
	public function index(): Response
	{
		$routeBuilder = $this->get(AdminUrlGenerator::class);

		return $this->redirect($routeBuilder->setController(ServiceCrudController::class)->generateUrl());
	}

	public function configureDashboard(): Dashboard
	{
		return Dashboard::new()
			->setTitle('<img src="/img/grimer.png">')
			->setFaviconPath('favicon.svg')
			;
	}

	public function configureMenuItems(): iterable
	{
		return [
			MenuItem::linkToCrud('Users', 'fa fa-user', User::class),
			MenuItem::linkToCrud('Cards', 'fa fa-credit-card', Card::class),
			MenuItem::linkToCrud('Services', 'fa fa-dungeon', Service::class),
			MenuItem::linkToCrud('Mocks', 'fa fa-pastafarianism', Mock::class),
		];
	}

	public function configureUserMenu(UserInterface $user): UserMenu
	{
		return parent::configureUserMenu($user)
			->setName($user->getUserIdentifier())
			->displayUserName(false)
			->displayUserAvatar(false)
			->addMenuItems([
				               MenuItem::linkToRoute('My Profile', 'fa fa-id-card', '...', ['...' => '...']),
				               MenuItem::linkToRoute('Settings', 'fa fa-user-cog', '...', ['...' => '...']),
				               MenuItem::section(),
				               MenuItem::linkToLogout('Logout', 'fa fa-sign-out'),
			               ]);
	}
}
