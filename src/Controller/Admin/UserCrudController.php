<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class UserCrudController extends AbstractCrudController
{

	private ?string $password;

	public function __construct(private UserPasswordHasherInterface $hasher, Security $security)
	{
		if (null !== $security->getUser()) {
			$this->password = $security->getUser()->getPassword();
		}
	}

	public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            EmailField::new('email'),
	        TextField::new('password')
		        ->setFormType(PasswordType::class)
	            ->setFormTypeOption('empty_data', '')
			    ->hideWhenUpdating(),
	        TextField::new('password')
		        ->setFormType(PasswordType::class)
		        ->setFormTypeOption('empty_data', '')
		        ->setRequired(false)
		        ->onlyWhenUpdating()
        ];

    }

	public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
	{
		// set new password with encoder interface
		if (method_exists($entityInstance, 'setPassword')) {
			$clearPassword = trim($this->get('request_stack')->getCurrentRequest()->request->all('User')['password']);

			// if user password not change save the old one
			if (isset($clearPassword) === true && $clearPassword === '') {
				$entityInstance->setPassword($this->password);
			} else {
				$encodedPassword = $this->hasher->hash($this->getUser(), $clearPassword);
				$entityInstance->setPassword($encodedPassword);
			}
		}

		parent::updateEntity($entityManager, $entityInstance);
	}
}
