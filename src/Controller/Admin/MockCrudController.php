<?php

namespace App\Controller\Admin;

use App\Entity\Mock;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CodeEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class MockCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Mock::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('description'),
            TextField::new('condition'),
            TextField::new('path'),
            ChoiceField::new('method')->setChoices(['GET' => 'GET', 'POST' => 'POST', 'PUT' => 'PUT']),
            IntegerField::new('code'),
            IntegerField::new('delay'),
	        CodeEditorField::new('body')->setNumOfRows(16),
        ];
    }
}
