<?php
namespace App\Message;

class DebinMessage
{
	public function __construct(private array $debin, private string $url) {}

	public function getDebin(): array
	{
		return $this->debin;
	}

	public function getUrl(): string
	{
		return $this->url;
	}
}
