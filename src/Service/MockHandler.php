<?php
namespace App\Service;

use App\Entity\Mock;
use App\Entity\Service;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class MockHandler
{
	public function handleServiceMocks(Service $service, Request $request): JsonResponse
	{
		$serializer = new Serializer([], [new JsonEncoder()]);
		$body = json_decode($request->getContent(), true);
		$path = $request->get('path') ? '/'.$request->get('path') : $request->getPathInfo();

		/** @var Mock[] $mocks */
		$mocks = $service->getMocks()->filter(function (Mock $mock) use ($path, $request) {
			return $mock->getMethod() == $request->getMethod() && str_contains($path, $mock->getPath());
		});

		$el = new ExpressionLanguage();
		foreach ($mocks as $mock) {
			try {
				$evaluation = !$mock->getCondition() || $el->evaluate($mock->getCondition(), ['body' => $body]);
			} catch (\Exception) {
				$evaluation = false;
			}
			if ($evaluation) {
				$responseBody = $mock->getBody();
				if ($body && $responseBody) {
					$this->replaceRecursive($body, $responseBody);
				}
				sleep($mock->getDelay());
				return new JsonResponse($serializer->decode($responseBody, 'json'), $mock->getCode());
			}
		}
		// Should implement Jsonapi errors schema
		return new JsonResponse('Mock not found for the request', 404);
	}

	public static function replaceRecursive($array, &$responseBody) {
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				self::replaceRecursive($value, $responseBody);
			} else {
				$responseBody = strtr($responseBody, ['{{'.$key.'}}' => $value]);
			}
		}
        $responseBody = strtr($responseBody, ['{{uuid}}' => uuid_create()]);
	}
}
