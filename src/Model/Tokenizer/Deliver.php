<?php

namespace App\Model\Tokenizer;

class Deliver
{
	public string $paymentMethodToken = '';
	public string $method = '';
	public array $headers = [];
	public string $url = '';
	public array $body = [];

	public function __construct($payment_method_token)
	{
		$this->paymentMethodToken = $payment_method_token;
	}
}
