<?php

namespace App\DataFixtures;

use App\Entity\Mock;
use App\Entity\Service;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $service = new Service();
	    $service->setId('example');
	    $service->setMocked(true);
        $mock = new Mock();
	    $mock->setMethod('POST');
	    $mock->setPath('payments');
	    $mock->setCode('201');
	    $mock->setBody('{"amount": "{{amount}}"}');
	    $mock->setCondition('body["amount"] == 200');
	    $service->setMocks([$mock]);
	    $manager->persist($service);
	    $manager->persist($mock);

	    $manager->flush();
    }
}
