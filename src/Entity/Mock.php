<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Mock
{
	#[ORM\Column(type: 'integer')]
	#[ORM\Id]
	#[ORM\GeneratedValue(strategy: 'AUTO')]
	private int $id;

	#[ORM\Column(type: 'string')]
	private string $description = '';

	#[ORM\Column(name: '`path`', type: 'string', nullable: true)]
	private ?string $path = '';

	#[ORM\Column(name: '`condition`', type: 'string', nullable: true)]
	private ?string $condition = '';

	#[ORM\Column(type: 'string')]
	private string $method = 'GET';

	#[ORM\Column(type: 'integer')]
	private int $code = 200;

	#[ORM\Column(name: '`body`', type: 'text', nullable: true)]
	private ?string $body = '';

	#[ORM\Column(type: 'integer')]
	private ?int $delay = 0;

	public function getId(): int
	{
		return $this->id;
	}

	public function getDescription(): string
	{
		return $this->description;
	}

	public function setDescription(string $description): void
	{
		$this->description = $description;
	}

	public function getPath(): ?string
	{
		return $this->path;
	}

	public function setPath(?string $path): void
	{
		$this->path = $path;
	}

	public function getCondition(): ?string
	{
		return $this->condition;
	}

	public function setCondition(?string $condition): void
	{
		$this->condition = $condition;
	}

	public function getMethod(): string
	{
		return $this->method;
	}

	public function setMethod(string $method): void
	{
		$this->method = $method;
	}

	public function getCode(): int
	{
		return $this->code;
	}

	public function setCode(int $code): void
	{
		$this->code = $code;
	}

	public function getBody(): ?string
	{
		return $this->body;
	}

	public function setBody(?string $body): void
	{
		$this->body = $body;
	}

	public function __toString(): string
	{
		return $this->description;
	}

	public function getDelay(): int
	{
		return $this->delay;
	}

	public function setDelay(int $delay): void
	{
		$this->delay = $delay;
	}
}
