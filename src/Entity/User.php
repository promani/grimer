<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class User implements PasswordAuthenticatedUserInterface, UserInterface
{
	#[ORM\Column(type: 'integer')]
	#[ORM\Id]
	#[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id;

	#[ORM\Column(type: 'string', nullable: true)]
    private ?string $password;

	#[ORM\Column(type: 'string')]
	#[Assert\NotBlank(message: 'Ingrese un email')]
    private ?string $email;

	#[ORM\Column(type: 'string', nullable: true)]
    private string $role = 'ROLE_USER';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->id;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    public function getUsername(): ?string
    {
        return $this->email;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getUserIdentifier()
    {
    	return $this->email;
    }

	public function getRole(): string
	{
		return $this->role;
	}

    public function getRoles(): array
    {
        return [$this->role];
    }

    public function setRole(string $role): void
    {
        $this->role = $role;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        return;
    }

    public function __toString(): string
    {
        return $this->email;
    }

}
