<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class Card
{
	#[ORM\Column(type: 'string')]
	#[ORM\Id]
	#[Assert\NotBlank]
	private string $token = '';

	#[Assert\Length(min: 16)]
	#[ORM\Column(type: 'string', nullable: true)]
	private string $pan = '';

	public function getToken(): string
	{
		return $this->token;
	}

	public function setToken(string $token): void
	{
		$this->token = $token;
	}

	public function getPan(): string
	{
		return $this->pan;
	}

	public function setPan(string $pan): void
	{
		$this->pan = $pan;
	}

}
