<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class Service
{
	#[ORM\Column(type: 'string')]
	#[ORM\Id]
	#[Assert\NotBlank]
	private string $id = '';

	// Only for tokenizer. Services sometimes are also receivers.
	#[ORM\Column(type: 'boolean')]
	private bool $mocked = true;

	/** @var Collection | Mock[] */
	#[ORM\ManyToMany(targetEntity: Mock::class)]
	#[ORM\JoinTable()]
    #[ORM\OrderBy(['condition' => 'DESC'])]
	private $mocks;

    public function __construct()
    {
        $this->mocks = new ArrayCollection();
    }

    public function getId(): string
	{
		return $this->id;
	}

	public function setId(string $id): void
	{
		$this->id = $id;
	}

	public function isMocked(): bool
	{
		return $this->mocked;
	}

	public function setMocked(bool $mocked): void
	{
		$this->mocked = $mocked;
	}

	/**
	 * @return Mock[]
	 */
	public function getMocks(): Collection
	{
		return $this->mocks;
	}

	public function setMocks($mocks): void
	{
		$this->mocks = $mocks;
	}

}
