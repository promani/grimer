<?php
namespace App\MessageHandler;

use App\Entity\Service;
use App\Message\DebinMessage;
use App\Service\MockHandler;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DebinMessageHandler implements MessageHandlerInterface
{
	public function __construct(private HttpClientInterface $client, private ManagerRegistry $registry) {}

	public function __invoke(DebinMessage $message)
	{
        $serializer = new Serializer([], [new JsonEncoder()]);
        $coelsa = $this->registry->getRepository(Service::class)->find('coelsa-callback');

        $el = new ExpressionLanguage();
        $body = $message->getDebin();
        $response = null;
        foreach ($coelsa->getMocks() as $mock) {
            try {
                $evaluation = !$mock->getCondition() || $el->evaluate($mock->getCondition(), ['body' => $body]);
            } catch (\Exception) {
                $evaluation = false;
            }
            if ($evaluation) {
                $responseBody = $mock->getBody();
                if ($body && $responseBody) MockHandler::replaceRecursive($body, $responseBody);
                $response = $serializer->decode($responseBody, 'json');
            }
        }

        if (!$response) {
            $detalle = $message->getDebin()['operacion']['detalle'];
            $response = [
                'operacion_original' => [
                    'id' => uuid_create(),
                    'tipo' => 'DEBINQR',
                    'qr_id_trx' => $detalle['qr_id_trx'],
                ],
                'respuesta' => [
                    'codigo' => 5700
                ]
            ];
        }

		$this->client->request('POST', $message->getUrl(), [ 'body' => $response ]);
	}
}
