<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServiceControllerTest extends WebTestCase
{
	public function testExampleService(): void
	{
		$client = static::createClient();

		$client->request('POST', '/services/example/payments', [], [], [], '{ "amount": 200 }');
		$response = $client->getResponse();
		$this->assertResponseIsSuccessful();
		$this->assertEquals(201, $response->getStatusCode());
		$content = json_decode($response->getContent(), true);
		$this->assertEquals(200, $content['amount']);
	}

	public function testNotFoundService(): void
	{
		$client = static::createClient();

		$client->request('POST', '/services/another/payments');
		$response = $client->getResponse();
		$this->assertEquals(404, $response->getStatusCode());
	}

	public function testExampleServiceNotFoundMock(): void
	{
		$client = static::createClient();

		$client->request('POST', '/services/example/payments', [], [], [], '{ "amount": 400 }');
		$response = $client->getResponse();
		$this->assertEquals(404, $response->getStatusCode());
		$this->assertEquals('"Mock not found for the request"', $response->getContent());
	}
}
