<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CoelsaControllerTest extends WebTestCase
{
	public function testDebin(): void
	{
		$client = static::createClient();

		$client->request('POST', '/coelsa/debin', [], [], [], file_get_contents(__DIR__ . '/data/debin.example'));
		$response = $client->getResponse();
		$this->assertResponseIsSuccessful();
		$this->assertEquals(201, $response->getStatusCode());
		$transport = $this->getContainer()->get('messenger.transport.async');
		$this->assertCount(1, $transport->getSent());
	}
}
